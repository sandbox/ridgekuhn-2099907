<?php

/* @file
 * Provides Rules integration for the Commerce Lot module
 */

function commerce_lot_rules_event_info() {
  $items = array(
    'commerce_lot_add_to_cart' => array(
      'label' => t('After a user adds a product to the cart, with a product in the Commerce Lot selected'),
      'module' => 'commerce_lot',
      'group' => t('Commerce Cart'),
      'variables' => array(
        'commerce_product' => array(
          'type' => 'commerce_product',
          'label' => t('The Commerce Lot product checked in the add to cart form'),
        ),
      ),
    ),
  );

  return $items;
}
