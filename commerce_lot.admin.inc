<?php

/**
 * @file
 * Administration page callbacks for the commerce_lot module.
 */

/**
 * Form builder. Configure Product Lots.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
 
function commerce_lot_admin_settings() {
  //Get Relation Types
  $options = relation_get_types_options();
  
  // Get enabled relation types from Commerce Lot admin settings (this form)
  $commerce_lot_relation_types = variable_get('commerce_lot_relation', $default = NULL);

  //List Relation Types as checkboxes
  $form['commerce_lot_relation'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select Relation Types available to Commerce Product Lot'),
    '#options' => $options,
    '#default_value' => $commerce_lot_relation_types,
  );

  $form['#submit'] = array('commerce_lot_admin_settings_submit');

  return system_settings_form($form);
}

/**
 * Process Commerce Lot settings submission. 
 *
 * @return array
 * An array of relation types enabled by this form. 
 * This array stored in variable_set('commerce_lot_relation_types') for
 * other modules to call.
 */

function commerce_lot_admin_settings_submit($form, $form_state) {
  // Clear stored Commerce Lot relation types from persistent variable
  // @todo: is this necessary?
  variable_del('commerce_lot_relation_types');

  // Create an array to store relation types enabled in
  // Commerce Lot admin settings
  $commerce_lot_relation_types = array();
  
  // For each relation type checkbox in the form,
  foreach ($form_state['values']['commerce_lot_relation'] as $key => $value) {
    // If the check box for a relation type is unchecked,
    if (!$value) {
      // Check if the relation type has the field commerce_lot_required
      $instance = field_info_instance('relation', 'commerce_lot_required', $key);

      // If the relation type has the field,
      if (!empty($instance)) {
        // Delete the field from the relation type
        field_delete_instance($instance, $field_cleanup = FALSE);
        
        // Echo confirmation message to watchdog
        watchdog('Commerce Lot', 'Deleted commerce_lot_required field
          from Relation Type: %key', array('%key' => $key));
      }
    }
    
    else {
      // Check if the relation type has the field commerce_lot_required
      $instance = field_info_instance('relation', 'commerce_lot_required', $key);
      
      // If the relation type does not have the field,
      if (empty($instance)) {
        // Create values for the field
        $instance = array(
          'field_name' => 'commerce_lot_required',
          'entity_type' => 'relation',
          'bundle' => $key,
          'label' => t('Product is Required in Lot'),
          'widget' => array(
            'type' => 'options_onoff'),
          'settings' => array('display_summary' => TRUE),
        );
        
        // Create the field on the relation type
        $instance = field_create_instance($instance);
        
        // Echo confirmation message to watchdog
        watchdog('Commerce Lot', 'Added commerce_lot_required field
          to Relation Type: %key', array('%key' => $key));
      }
      
      // Add the name ($value) of the relation type to
      // $commerce_lot_relation_types for storage in the persistent
      // variable commerce_lot_relation_types. Other Commerce Lot
      // functions will call this to get enabled relation types.
      $commerce_lot_relation_types[] = $value;
      }
  }
    
  // Save $commerce_lot_relation_types as a persistent variable
  variable_set('commerce_lot_relation_types', $commerce_lot_relation_types);
} 