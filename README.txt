
CONTENTS OF THIS FILE
---------------------

 * About this Module
 * Note from the author
 * Module Dependencies
 * Commerce Contrib support
 * Alternatives
 * What does Commerce Lot do?
 * Differences between similar modules
 * Nomenclature
 * Examples
 * Roadmap
 * Installation
 * Changelog
 

ABOUT THIS MODULE
-----------------

Using Commerce Lot, you can create flexible solutions for when you need to
sell multiple products together.

NOTE FROM THE AUTHOR
--------------------

This is my first Drupal module ever! \(^_^)/

MODULE DEPENDENCIES
-------------------

 * Commerce https://drupal.org/project/commerce
 * Relation https://drupal.org/project/relation
 * Relation Add https://drupal.org/project/relation_add

COMMERCE CONTRIB SUPPORT
------------------------

Currently, this module has not been tested to work with any other module
outside of its dependencies. See ROADMAP.

ALTERNATIVES
------------

This module is a Relation module based alternative to
 * Commerce Product Add-On https://drupal.org/project/commerce_pado, and
 * Commerce Product Bundle https://drupal.org/project/commerce_product_bundle
 
WHAT DOES COMMERCE LOT DO?
----------------

When you create a Commerce Lot enabled product and add other products to the
lot, a relation is automatically generated between the two entities. Since
Relation module stores relations as entities, we can add extra fields
to affect how each product in the lot is processed by Commerce, 
e.g., products can be optional upsells or required purchases if a customer
buys a Commerce Lot product, etc.

Since the module must check for Relation entities, this adds overhead to the
Drupal engine. If you do not need the kind of extra features enabled by
Relation entities, the above-mentioned modules may suit your needs better.
Commerce Product Bundle is a mature project and already contains many
features planned for this module.

DIFFERENCES BETWEEN SIMILAR MODULES
-----------------------------------

The main difference between this and Commerce Product Lot/Commerce PADO is 
that it relies on Relation module entities to define relationships between 
products rather than using a simple product reference or entity reference. 
Since we can have extra fields on relation entities, we can use these to extend 
how Commerce processes ordering multiple products together. For example, say 
you have an mp3 store and want to sell a compilation of individual songs 
(a Commerce Lot) which already exist as product entities your store, and give 
customers an option in the album's Add To Cart form to buy a t-shirt at a 
reduced price if they purchase the album.
(This example isn't possible yet with Commerce Lot, but this 
is one of the goals of the module.)

Note: Since the module must check for Relation entities, this adds overhead to 
the Drupal engine. If you do not need the kind of extra features enabled by 
Relation entities, the above-mentioned modules may fit your needs better. 
Commerce Product Bundle is a mature project and already contains many 
features planned for this module. 

NOMENCLATURE
------------

 * Lot
   A reference to a set of Relation entities of types enabled by Commerce Lot.
   A Lot's inner relations share a super-product as endpoint 0 and 
   different sub-products as endpoint(s) >0 in the relations.
   
 * Super-Product
   The product with endpoint 0 in the relation. This product will host
   the sub-products in its add to cart form.
  
 * Sub-Product
   The produt with endpoint(s) <0 in the relation. These products will appear
   in the super-product's add to cart form.
   

EXAMPLES
--------

Updated as features added

 * You want to give a customer the option to, or force a customer to, add a
   product to the Commerce Cart from another product's add to cart form.  
 
ROADMAP
-------

Last updated 08/29/2013

Below are planned features, bug fixes, Commerce Contrib module support
(as submodules when necessary), etc.

BUG?: Commerce Lot products combine in cart even if 
          attempt to combine like items is unchecked in settings. 
          (added products combine (or don't) as expected)
ETC?: Disable options in relation types, edit forms that may break
          functionality
FEAT: Cart price calculation per product in lot via Relation field checkbox
FEAT: Price overrides per product via Relation field checkbox
FEAT: Quantity overrides per product via Relation field checkbox
FEAT: Combine product into Commerce Lot's line item
          via Relation field checkbox
          (or, is this moot because if the Lot price overrides products, the
          line item price would be 0 and be removed from the cart? Hrmm...)
FEAT: Support relation arity >2 for product variations
FEAT: Create submodule to install generic fields, relation type, product type
FEAT: Option to delete relation entities created with this module when
          module is uninstalled (if this is even possible?)
CTRB: Support Commerce Stock module
CTRB: Support Commerce Physical Product module
CTRB: Support Commerce Shipping
CTRB: Support Commerce Product Attributes module
CTRB: Support Commerce Product Options module
CTRB: Support Commerce File module

INSTALLATION
------------

See INSTALL.txt

CHANGELOG
---------

See CHANGELOG.txt