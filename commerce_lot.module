<?php

/**
 * @file
 * Add products to another product's add to cart form.
 *
 * Using Commerce Lot, you can create flexible solutions for when you need to
 * sell multiple products together.
 *
 * This module is a Relation module based alternative to
 * @link https://drupal.org/project/commerce_pado @endlink, and
 * @link https://drupal.org/project/commerce_product_bundle @endlink.
 *
 * When you create a Commerce Lot enabled product and add other products to the
 * lot, a relation is automatically generated between the two entities. Since
 * Relation module stores relations as entities, we can add extra fields
 * to affect how each product in the lot is processed by Commerce, 
 * e.g., products can be optional upsells or required purchases if a customer
 * buys a Commerce Lot product, etc.
 * 
 * Since the module must check for Relation entities, this adds overhead to the
 * Drupal engine. If you do not need the kind of extra features enabled by
 * Relation entities, the above-mentioned modules may suit your needs better.
 * Commerce Product Bundle is a mature project and already contains many
 * features planned for this module.
 */

/**
 * Implementation of hook_menu(). 
 * Generates Commerce Product Lot menu links at admin/
 */
function commerce_lot_menu() {
  $items['admin/commerce/config/commerce_lot'] = array(
    'title' => 'Commerce Lot',
    'description' => 'Commerce Lot settings',
    'position' => 'right',
    'weight' => -5,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  
  $items['admin/commerce/config/commerce_lot'] = array(
    'title' => 'Commerce Lot settings',
    'description' => 'Select Relation Types to be used for Commerce Lot products',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_lot_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'commerce_lot.admin.inc',
  );
  
  return $items;
}

/**
 * Implements commerce_lot_load()
 * 
 * Gets Commerce Lot information about a product.
 *
 * @param integer $product_id
 *   The entity (product) id of a Commerce product entity
 *
 * @return array $commerce_lot_load
 *   The array created by the function
 */
function commerce_lot_load($product_id) {
  // Create an array of entity (relation) ids from each relation with
  // $product_id as an endpoint. 
  $rids = array_keys(relation_query('commerce_product', $product_id)->execute());

  // If there are relations with $product_id as an endpoint,
  if (!empty($rids)) {
    // Get relation types enabled in Commerce Lot admin settings
    $commerce_lot_relation_types = variable_get('commerce_lot_relation_types', $default = NULL);
    
    // Create an array to store relation objects
    $relations = array();
    
    // For each relation id in $rids,
    foreach ($rids as $rid) {
      // Load the relation object into $relations
      $relations[] = relation_load($rid, $vid = NULL, $reset = FALSE);
    }
    
    // Create an array to store relation objects of products in
    // the product's Commerce Lot
    $commerce_lot_relations = array();
    
    // Create an array to store entity (product) ids of products in
    // $product's Commerce Lot. 
    $commerce_lot_endpoint_ids = array();

    // For each $relation object in $relations,
    foreach ($relations as $relation) {   
      // If the relation type is enabled in Commerce Lot admin settings,
      if (in_array($relation->relation_type, $commerce_lot_relation_types)) {       
        
        // If the relation's first endpoint entity_id matches $product's
        // product id, this is a Commerce Lot. 
        if ($relation->endpoints[LANGUAGE_NONE][0]['entity_id'] == $product_id) {
          // For each endpoint in this relation,
          foreach ($relation->endpoints[LANGUAGE_NONE] as $key => $commerce_lot_relation) {
            // Skip the first $key, since it IS the Commerce Lot and we need
            // entity ids of products IN the Commerce Lot 
            if ($key != 0) {
              // Add the $relation object to $commerce_lot_relations, keyed by
              // relation id
              $commerce_lot_relations[$relation->rid] = $relation;
            
              // Add the entity ids of each endpoint into the
              // the $commerce_lot_endpoint_ids array, keyed by relation id
              $commerce_lot_endpoint_ids[$relation->rid] = $commerce_lot_relation['entity_id'];
              
              // Set additional values
              $commerce_lot_in_lot = TRUE;
              $commerce_lot_super_product = TRUE;
              $commerce_lot_sub_product = FALSE;
            }
          }
        }
        // If the relation's first endpoint entity_id does NOT match $product's
        // product id, this is a product in the Commerce Lot
        if ($relation->endpoints[LANGUAGE_NONE][0]['entity_id'] != $product_id) {
        
          // Add the $relation object to $commerce_lot_relations, keyed by
          // relation id
          $commerce_lot_relations[$relation->rid] = $relation;
        
          // Add the entity id of the first endpoint (the Commerce Lot) into the
          // the $commerce_lot_endpoint_ids array, keyed by relation id
          $commerce_lot_endpoint_ids[$relation->rid] = $relation->endpoints[LANGUAGE_NONE][0]['entity_id'];

          // Set additional values
          $commerce_lot_in_lot = TRUE;
          $commerce_lot_super_product = FALSE;
          $commerce_lot_sub_product = TRUE;
        }
      } 
    }
    
    // Create an array for the function to return results to
    $commerce_lot_load = array();
    
    // Set Commerce Lot status of product
    
    switch ($commerce_lot_in_lot == TRUE) {
      case (isset($commerce_lot_in_lot)):
        $commerce_lot_load['in_lot'] = $commerce_lot_in_lot;

      case (isset($commerce_lot_super_product)):
        $commerce_lot_load['super_product'] = $commerce_lot_super_product;
      
      case (isset($commerce_lot_sub_product)):
        $commerce_lot_load['sub_product'] = $commerce_lot_sub_product;
      
      case (!empty($commerce_lot_relations)):
        // Add relation objects of products in the Commerce Lot to the array,
        // keyed by relation id
        $commerce_lot_load['relations'] = $commerce_lot_relations;
    
      // If $commerce_lot_endpoint_ids is not empty,
      case (!empty($commerce_lot_endpoint_ids)):
        // Add the entity ids of products in the Commerce Lot to the array,
        // keyed by relation id
        $commerce_lot_load['endpoint_ids'] = $commerce_lot_endpoint_ids;
      break;
    }
    
    // Return results
    return $commerce_lot_load;
    
  } // End if (!empty($rids))
}

/*
 * Implements hook_entity_info_alter().
 *
 * Adds a view mode to the product type display.
 */

function commerce_lot_entity_info_alter(&$info) {
  $info['commerce_product']['view modes']['commerce_lot'] = array(
    'label' => t('Commerce Lot'),
    'custom settings' => TRUE,
    );
}

/**
 * Implementation of hook_form_FORM_ID_alter().
 */

function commerce_lot_form_commerce_cart_add_to_cart_form_alter(&$form, &$form_state) {
  // Load related products  
  // Get the default product
  $product = $form_state['default_product'];
  
  // Get the $product's product id
  $product_id = $product->product_id;
  
  // Get Commerce Lot relation data
  $commerce_lot_load = commerce_lot_load($product_id);

  // If commerce_lot_load returned data, 
  if (!empty($commerce_lot_load)) {
    // Create an array to store objects of products in the Commerce Lot
    $commerce_lot_products = array();
    
    // For each product in our Commerce Lot,
    foreach ($commerce_lot_load['endpoint_ids'] as $rid => $commerce_lot_product) {
      // Load the product object into the $commerce_lot_products array,
      // keyed by relation id via function commerce_product_load
      $commerce_lot_products[$rid] = commerce_product_load($commerce_lot_product);
    }

    // Add Commerce Lot Products to $form_state
    $form_state['commerce_lot_products'] = $commerce_lot_products;
    
    // Add a submit handler to $form
    $form['#submit'][] = 'commerce_lot_add_to_cart_submit';
      
    // For each product object in $commerce_lot_products, keyed by relation id
    // via function commerce_lot_load,
    foreach ($commerce_lot_products as $rid => $commerce_lot_product) {
      // Get the renderable array for each product in the Commerce Lot
      // @todo: Create View Mode (make view mode configurable by user)
      $commerce_lot_product_view = entity_view('commerce_product', 
        array($commerce_lot_product->product_id => $commerce_lot_product), 'commerce_lot', $langcode = NULL, TRUE);
      
      // If commerce_lot_required is checked,
      if ($commerce_lot_load['relations'][$rid]->commerce_lot_required[LANGUAGE_NONE][0]['value'] == 1) {
        // Add products in Commerce Lot to add to cart form,
        $form[$commerce_lot_product->sku] = array(
        '#type' => 'checkbox',
        // Render the view and sanitize tags
        '#title' => filter_xss(drupal_render($commerce_lot_product_view), $allowed_tags = array('div', 'ul', 'li', 'a')),
        // Select the product's checkbox
        '#default_value' => 1,
        // Disable unselect
        '#disabled' => 1,
        );
            
      }
      
      else {      
        // Add products in Commerce Lot to add to cart form,
        $form[$commerce_lot_product->sku] = array(
        '#type' => 'checkbox',
        // Render the view and sanitize tags
        '#title' => filter_xss(drupal_render($commerce_lot_product_view), $allowed_tags = array('div', 'ul', 'li', 'a')),
        );
      }
    }
  }
}

/*
 * Submit handler added to the Add To Cart form.
 * Adds checked Commerce Lot products to Cart using Rules.
 */
function commerce_lot_add_to_cart_submit($form, &$form_state) {
  // Get the products in the Commerce Lot
  $commerce_lot_products_submit = $form_state['commerce_lot_products'];
  
  // For each product in the Commerce Lot,
  foreach ($commerce_lot_products_submit as $commerce_lot_product_submit) {
    // If the product was checked in the add to cart form,
    if ($form_state['values'][$commerce_lot_product_submit->sku] == 1) {
      // Add the product to cart using included commerce_lot.rules
      rules_invoke_event('commerce_lot_add_to_cart', $commerce_lot_product_submit);
    }
  }
}

/**
 * Implementation of hook_form_FORM_ID_alter()
 */
function commerce_lot_form_views_form_commerce_cart_form_default_alter(&$form, &$form_state) {
  // Get the $order from the cart order
  $order = $form_state['order'];
  
  // Get line items in the order
  $line_items = $form_state['line_items'];

  // For each Remove button on line items in cart $form,
  foreach ($form['edit_delete'] as $form_edit_delete_key => $value) {
    if ($form_edit_delete_key != '#tree') {
      // Get the line item id from the $form
      $line_item_id = $form['edit_delete'][$form_edit_delete_key]['#line_item_id'];

      // Get the line item object from $form_state
      $line_item = $form_state['line_items'][$line_item_id];

      // Get the product ids from the line item
      $product_ids = $line_item->commerce_product[LANGUAGE_NONE];

      // For each product in the line item,
      foreach ($product_ids as $product_id) {
        // Run Commerce Lot Load
        $commerce_lot_load = commerce_lot_load($product_id['product_id']);

        // If the product is a Commerce Lot sub-product
        if ($commerce_lot_load['sub_product'] == TRUE) {
        
        // For each Commerce Lot relation object,
        foreach ($commerce_lot_load['relations'] as $rid => $value)
          // If the commerce_lot_required field is checked,
          if ($commerce_lot_load['relations'][$rid]->commerce_lot_required[LANGUAGE_NONE][0]['value'] == 1) {
            // Disable the Remove button in the cart form.
            $form['edit_delete'][$form_edit_delete_key]['#disabled'] = 1;
          }
        }
      }
    }
  }
}

/*
 * Implementation of hook_commerce_cart_product_remove().
 */
function commerce_lot_commerce_cart_product_remove($order, $product, $quantity, $line_item) {
  // Get the $product's product id
  $product_id = $product->product_id;

  // Get Commerce Lot relation data
  $commerce_lot_load = commerce_lot_load($product_id);

  // If the $product has Commerce Lot endpoints,
  if ($commerce_lot_load['in_lot'] == TRUE) {
      // If the product is a Commerce Lot super-product,
    if ($commerce_lot_load['super_product'] == TRUE) {
      // If there are line items in the $order,
      if (isset($order->commerce_line_items[LANGUAGE_NONE])) {
        // For each line item in the $order,
        foreach ($order->commerce_line_items[LANGUAGE_NONE] as $key => $order_line_item_reference) {
          // If the line item is not the one we just deleted from the cart,
          // @todo: Is this necessary? Throws error, commented out for now.
          // if ($order_line_item_reference['line_item_id'] != $line_item['line_item_id']) {
            // Load the line item object 
            $order_line_items = commerce_line_item_load($order_line_item_reference['line_item_id']);     
    
            // If there are products in the line item,
            if (isset($order_line_items->commerce_product[LANGUAGE_NONE])) {
              // For each commerce_product found in the line item,
              foreach ($order_line_items->commerce_product[LANGUAGE_NONE] as $order_line_item) {
                // If the line item contains a sub-product which is related to the
                // super-$product just deleted,
                if (in_array($order_line_item['product_id'], $commerce_lot_load['endpoint_ids'])) {
                  // Remove the line item from the order.
                  commerce_cart_order_product_line_item_delete($order, $order_line_item_reference['line_item_id'], $skip_save = FALSE); 
                }
              }
            }
          // }
        }
      }
    }
  }
}	